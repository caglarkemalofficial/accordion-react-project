import SingleQuestion from './SingleQuestion'

const Questions = ({ question }) => {
  return (
    <section className="container">
      <h1>Questions</h1>
      {question.map((questions) => {
        return <SingleQuestion key={questions.id} {...questions} />
      })}
    </section>
  )
}
export default Questions
