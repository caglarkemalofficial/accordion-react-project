import { useState } from 'react'
const SingleQuestion = ({ title, info }) => {
  const [questionInfo, setQuestionInfo] = useState(false)
  return (
    <article className="question">
      <header>
        <h5>{title}</h5>
        <button
          type="button"
          className="question-btn"
          onClick={() => setQuestionInfo(!questionInfo)}>
          +
        </button>
      </header>
      {questionInfo && <p>{info}</p>}
    </article>
  )
}
export default SingleQuestion
